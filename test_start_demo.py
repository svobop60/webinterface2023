from itertools import permutations
import numpy as np

NUM_BALLS = 3
demoinfor = 15

def assign_drops(refs, drops):
    def dist(ref, drop):
        return (ref[0]-drop[0])**2 + (ref[1]-drop[1])**2
    best_d = np.inf
    best_perm = None
    perms = list(permutations(range(3)))
    for p in perms:
        d = 0
        for i in range(3):
            d += dist(refs[i], drops[p[i]])
        if d < best_d:
            best_d = d
            best_perm = p
    return best_perm, best_d


def start_demo(drops):
    def get_refs(phi):
        refs = []
        r = demoinfor
        for i in range(NUM_BALLS):
            refs.append([r*np.cos(phi+i*2*np.pi/3), r*np.sin(phi+i*2*np.pi/3)])
        return refs

    # find angle phi
    loss = np.inf
    best_phi = 0
    best_perm = None
    steps = 100
    step = 2*np.pi/steps
    for i in range(steps):
        phi = i*step
        refs = get_refs(phi)
        perm, d = assign_drops(refs, drops)
        if d < loss:
            loss = d
            best_phi = phi
            best_perm = perm
    return best_phi, best_perm

if __name__ == "__main__":
    drops = [[6,8], [7,-10], [-18,-3]]
    phi, perm = start_demo(drops)
    print(phi, perm)
    