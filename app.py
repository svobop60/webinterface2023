#!/usr/bin/env python3

from eventlet.green import socket
import struct
import logging
import subprocess
from threading import Lock, Thread
from flask import Flask, render_template, make_response, Response, jsonify, redirect, url_for
from flask_socketio import SocketIO, emit
from PIL import Image
import numpy as np
from enum import IntEnum
import requests
import time
import cv2
import click
import urllib.request
import math
from itertools import permutations

# application runs on a web server (localhost:5000) using Flask
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
# I/O socket for communication between browser and app
socketio = SocketIO(app, async_mode="threading")

# thread for receiving data from Simulink model
thread = None
thread_lock = Lock()

# socket for communication with Simulink model
simulink_socket = None

# number of objects
NUM_BALLS = 3

y1,x1 = np.meshgrid([35,15,-5,-25],[35,15,-5,-25])
y1 = np.reshape(y1, (16,1))
x1 = np.reshape(x1, (16,1))
y2,x2 = np.meshgrid([25,5,-15,-35],[25,5,-15,-35])
y2 = np.reshape(y2, (16,1))
x2 = np.reshape(x2, (16,1))

TRANSDUCER_HEIGHT = 0.078

H_UNROT_FILENAME = '/home/pi/H_unrot.bin'
CORRECTION_FILENAME = '/home/pi/correction.bin'
HEIGHT_FILENAME = '/home/pi/height.bin'

# size of the manipulation area
area_size = 100.0

simulink_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
simulink_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
simulink_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
simulink_socket.bind(('',25001))

# enumeration of platform modes
class ModeEnum(IntEnum):
    NONE = 0
    FOCAL = 1
    DROPS = 2
    DROPS3 = 3

# platform status
class Status:
    def __init__(self):
        self.mode = ModeEnum.NONE
        self.focal_reference = [[25*np.cos(a), 25*np.sin(a)] for a in np.linspace(0, 2*np.pi, num=NUM_BALLS, endpoint=False)]
        self.drop_position = [[0,0] for i in range(NUM_BALLS)]
        self.demo = 0

status = Status()

class DemoInfo:
    def __init__(self):
        self.phi = 0
        self.r = 15
        self.phi_step = math.pi/8
        self.perm = [0,1,2]
        self.moving = 0

    def get_refs(self):
        refs = []
        if status.demo == 1:
            for i in range(NUM_BALLS):
                refs.append([self.r*math.cos(self.phi+i*2*math.pi/3), self.r*math.sin(self.phi+i*2*math.pi/3)])
            refs = [refs[self.perm[i]] for i in range(3)]
        elif status.demo == 2:
            for i in range(NUM_BALLS):
                refs.append([self.r*math.cos(self.phi+i*2*math.pi/3), self.r*math.sin(self.phi+i*2*math.pi/3)])
            refs = [refs[self.perm[i]] if i==self.moving else status.drop_position[i] for i in range(3)]
        return refs

demoinfo = DemoInfo()

def readPosThread():
    print('start background task ...')

    ball_bytes = NUM_BALLS * 8
    message_bytes = np.max([ball_bytes])

    while True :
        message,address = simulink_socket.recvfrom(message_bytes)
        if status.mode == ModeEnum.DROPS:
            num_drops = 1
            try:           
                data = struct.unpack("=" + str(num_drops*2) + "f", message)
                status.drop_position = [[data[2*i], data[2*i+1]] for i in range(num_drops)]
                #print(status.drop_position)
            except:
                print("Received partial/incorrect packet")
        elif status.mode == ModeEnum.DROPS3:
            num_drops = 3
            try:           
                data = struct.unpack("=" + str(num_drops*2) + "f", message)
                status.drop_position = [[data[2*i], data[2*i+1]] for i in range(num_drops)]
                if status.demo > 0:
                    move_refs()
                #print(status.drop_position)
            except:
                print("Received partial/incorrect packet")
        web_update()

def move_refs():
    def dist(pos, ref):
        return math.sqrt((pos[0]-ref[0])**2 + (pos[1]-ref[1])**2)
    if status.demo == 1: # circle
        move_ref = True
        min_dist = 5
        
        for i in range(NUM_BALLS):
            pos = status.drop_position[i]
            ref = status.focal_reference[i]
            if dist(pos, ref) > min_dist:
                move_ref = False
                break
        if move_ref:
            demoinfo.phi += demoinfo.phi_step
            if demoinfo.phi > 2*math.pi:
                demoinfo.phi -= 2*math.pi
            status.focal_reference = demoinfo.get_refs()
            simulink_update()
    elif status.demo == 2:
        move_ref = True
        min_dist = 5

        refs = []
        for i in range(NUM_BALLS):
            refs.append([demoinfo.r*math.cos(demoinfo.phi+i*2*math.pi/3), demoinfo.r*math.sin(demoinfo.phi+i*2*math.pi/3)])
        refs = [refs[demoinfo.perm[i]] for i in range(3)]

        for i in range(NUM_BALLS):
            pos = status.drop_position[i]
            ref = refs[i]
            if dist(pos, ref) > min_dist:
                move_ref = False
                break
        pos = status.drop_position[demoinfo.moving]
        ref = status.focal_reference[demoinfo.moving]

        if not move_ref and dist(pos, ref) < min_dist:
            demoinfo.moving = demoinfo.moving + 1 if demoinfo.moving < NUM_BALLS-1 else 0
            #print("change moving to ", demoinfo.moving)
            status.focal_reference = demoinfo.get_refs()
            simulink_update()

        elif move_ref:
            demoinfo.phi += demoinfo.phi_step
            if demoinfo.phi > 2*math.pi:
                demoinfo.phi -= 2*math.pi
            status.focal_reference = demoinfo.get_refs()
            simulink_update()


def assign_drops(refs, drops):
    def dist(ref, drop):
        return (ref[0]-drop[0])**2 + (ref[1]-drop[1])**2
    best_d = np.inf
    best_perm = None
    perms = list(permutations(range(3)))
    for p in perms:
        d = 0
        for i in range(3):
            d += dist(refs[i], drops[p[i]])
        if d < best_d:
            best_d = d
            best_perm = p
    return best_perm, best_d


def start_demo():
    drops = status.drop_position
    def get_refs(phi):
        refs = []
        r = demoinfo.r
        for i in range(NUM_BALLS):
            refs.append([r*np.cos(phi+i*2*np.pi/3), r*np.sin(phi+i*2*np.pi/3)])
        return refs

    # find angle phi
    loss = np.inf
    best_phi = 0
    best_perm = None
    steps = 100
    step = 2*np.pi/steps
    for i in range(steps):
        phi = i*step
        refs = get_refs(phi)
        perm, d = assign_drops(refs, drops)
        if d < loss:
            loss = d
            best_phi = phi
            best_perm = perm
    return best_phi, best_perm


def get_my_ip():
    return subprocess.check_output(['hostname', '--all-ip-addresses']).decode().split()

def responseImage(image):
    response = make_response(image)
    response.headers['Content-Type'] = 'image/png'
    return response

# function for updating web interface
def web_update():
    if status.mode == ModeEnum.FOCAL:
        socketio.emit('update', {'reference': status.focal_reference}, broadcast = True)
    elif status.mode == ModeEnum.DROPS or status.mode == ModeEnum.DROPS3:
        socketio.emit('update', {'position': status.drop_position, 'reference': status.focal_reference}, broadcast = True)

def simulink_update():
    data = bytearray()
    data.extend(struct.pack("BB", status.mode, status.demo))
    #print("Sending data to Simulink: {}".format(data))
    if status.mode == ModeEnum.FOCAL or status.mode == ModeEnum.DROPS:
        for i in range(1):
            ref = status.focal_reference[i]
            data.extend(struct.pack("ff", ref[0], ref[1]))
    elif status.mode == ModeEnum.DROPS3:
        for i in range(3):
            ref = status.focal_reference[i]
            data.extend(struct.pack("ff", ref[0], ref[1]))
        #print(status.focal_reference)
    #print("Sending data to Simulink: ", data)
    #print(len(data))
    simulink_socket.sendto(data, ("127.0.0.1",25000))
    
@socketio.on('connect')
def onconnect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(target=readPosThread)

# responds to the web interface requesting update
@socketio.on('request_update')
def request_update():
    web_update()

@socketio.on('set_demo')
def set_demo(d):
    #print("Demo set to {}".format(d))
    status.demo = int(d)
    print("starting demo ", status.demo)
    phi, perm = start_demo()
    demoinfo.phi = phi
    demoinfo.perm = perm
    status.focal_reference = demoinfo.get_refs()
    web_update()
    simulink_update()

@socketio.on('focal_reference')
def focal_reference(id, position):
    #print("Object {} moved to {}".format(id,position))
    status.demo = 0
    status.focal_reference[id][0] = position[0]
    status.focal_reference[id][1] = position[1]
    web_update()
    simulink_update()

@socketio.on('log')
def log(*args):
    print(*args)

@app.route('/<service>/<command>')
def systemd(service, command):
    if not service in ('Focal', 'Drops', 'Drops3'):
        raise RuntimeError("Unallowed service {}".format(service))
    if not command in ('start', 'stop', 'status', 'restart'):
        raise RuntimeError("Unallowed command {}".format(command))

    with subprocess.Popen(["systemctl", command, service], stdout=subprocess.PIPE) as proc:
        if service in ('Focal') and command in ('start','restart'):
            time.sleep(0.5)
        return Response(proc.stdout.read().decode(), mimetype='text/plain')

@app.route('/poweroff')
def poweroff():
    subprocess.call(["poweroff"])
    return "OK"

@app.route('/reboot')
def reboot():
    subprocess.call(["reboot"])
    return "OK"

@app.route('/ip')
def list_ip():
    return jsonify(get_my_ip())

@app.route('/focal')
def balls():
    status.mode = ModeEnum.FOCAL
    status.demo = 0
    time.sleep(2.0)
    #time.sleep(0.5)
    simulink_update()
    return render_template("focal.html", area_size=area_size)

@app.route('/drops')
def drops():
    status.mode = ModeEnum.DROPS
    status.demo = 0
    time.sleep(2.0)
    #time.sleep(0.5)
    simulink_update()
    return render_template("drops.html", area_size=area_size)

@app.route('/drops3')
def drops3():
    status.mode = ModeEnum.DROPS3
    status.demo = 0
    time.sleep(2.0)
    #time.sleep(0.5)
    simulink_update()
    return render_template("drops3.html", area_size=area_size)

@app.route('/')
def index():
    status.mode = ModeEnum.NONE
    #systemd('Focal', 'stop')
    return render_template("index.html")

@click.command()
@click.option('--area-size', type=click.FLOAT, default=100, help="Size of the manipulation area that is shown in the webpage")
def main(**kwargs):
    global area_size
    area_size = kwargs["area_size"]
    global app_running
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)
    socketio.run(app, "::", debug=True, use_reloader=False)

if __name__ == '__main__':
    main()
